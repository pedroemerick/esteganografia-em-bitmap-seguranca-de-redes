/**
* @file   cabecalho_img.h
* @brief  Arquivo com os atributos e metodos da classe CabecalhoImg
* @author Pedro Emerick (p.emerick@live.com)
* @since  16/08/2018
* @date   24/08/2018
*/

#ifndef CABECALHO_IMG_H
#define CABECALHO_IMG_H

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <string>
using std::string;

/**
* @class   CabecalhoImg cabecalho_img.h
* @brief   Classe que representa o cabecalho da imagem bitmap
* @details O cabecalho da imagem bitmap são os dados importantes para a manipulação de um bmp
*/ 
class CabecalhoImg 
{
    private:
        int biSize;             /**< Tamanho do cabeçalho */
        int biWidth;            /**< Largura da imagem em pixels */
        int biHeight;           /**< Altura da imagem em pixels */
        short int biPlanes;     /**< Número de planos de imagem na imagem. É sempre igual a 1 */
        short int biBitCount;   /**< Bits por pixel */
        int biCompression;      /**< Tipo de compactação */
        int biSizeImage;        /**< Tamanho da imagem - pode ser zero para imagens não compactadas */
        int biXPelsPerMeter;    /**< Resolução preferida em pixels por metro */
        int biYPelsPerMeter;    /**< Resolução preferida em pixels por metro */
        int biClrUsed;          /**< Entradas do Mapa de Cores Numéricas que são realmente usadas */    
        int biClrImportant;     /**< Número de cores significativas */ 

    public:
        /** @brief Construtor padrao */
        CabecalhoImg ();

        /** @brief Carrega o cabecalho da imagem */
        void carregaCabecalho (ifstream &imagem);

        /** @brief Grava o cabecalho da imagem */
        void gravaCabecalho (ofstream &imagem);

        /** @brief Imprimi os dados do cabecalho de foram organizada */
        void toString ();

        /** @brief Retorna a altura da imagem em pixels */
        int getBiHeight ();

        /** @brief Retorna a largura da imagem em pixels */
        int getBiWidth ();
};

#endif