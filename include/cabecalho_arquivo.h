/**
* @file   cabecalho_arquivo.h
* @brief  Arquivo com os atributos e metodos da classe CabecalhoArq
* @author Pedro Emerick (p.emerick@live.com)
* @since  16/08/2018
* @date   24/08/2018
*/

#ifndef CABECALHO_ARQUIVO_H
#define CABECALHO_ARQUIVO_H

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <string>
using std::string;

/**
* @class   CabecalhoArq cabecalho_arquivo.h
* @brief   Classe que representa o cabecalho de um arquivo bitmap
* @details Os atributos de um cabecalho de um arquivo bitmap são o tipo, tamanho, 
           bits reservados e offbits
*/ 
class CabecalhoArq
{
    private:
        char bfType [2];            /**< Códigos ASCII para os caracteres 'B' e 'M' */
        int bfSize;                 /**< Tamanho do arquivo em bytes. */
        short int bfReserved1;      /**< Reservado para futuras extensões da definição de formato - Não usado */
        short int bfReserved2;      /**< Reservado para futuras extensões da definição de formato - Não usado */
        int bfOffBits;              /**< Deslocamento para iniciar o Pixel Data */

    public:
        /** @brief Construtor padrao */
        CabecalhoArq ();
        
        /** @brief Carrega o cabecalho do arquivo de um bitmap */
        void carregaCabecalho (ifstream &imagem);

        /** @brief Grava o cabecalho do arquivo de um bitmap */
        void gravaCabecalho (ofstream &imagem);

        /** @brief Imprimi os dados do cabecalho de foram organizada */
        void toString ();
};

#endif