/**
* @file   bmp.h
* @brief  Arquivo com os atributos e metodos da classe BMP
* @author Pedro Emerick (p.emerick@live.com)
* @since  16/08/2018
* @date   24/08/2018
*/

#ifndef BMP_H
#define BMP_H

#include "cabecalho_arquivo.h"
#include "cabecalho_img.h"
#include "rgb.h"

#include <string>
using std::string;

#include <vector>
using std::vector;

/**
* @class   BMP bmp.h
* @brief   Classe que representa uma imagem bitmap
* @details Os atributos de um RGB são as cores vermelho, verde e azul
*/ 
class BMP 
{
private:
    string arquivo_entrada;             /**< Endereço da imagem para leitura */
    CabecalhoArq ca;                    /**< Cabeçalho do arquivo bitmap*/
    CabecalhoImg ci;                    /**< Cabeçalho da imagem bitmap */
    vector<vector<RGB>> mapa_pixels;    /**< Mapa de pixels da imagem bitmap */
    string mensagem;                    /**< Mensagem a ser gravada ou mensagem lida do bitmap */

public:
    /** @brief Construtor parametrizado */
    BMP (string entrada, CabecalhoArq ta, CabecalhoImg ti);

    /** @brief Construtor padrao */
    BMP ();

    /** @brief Carrega a imagem bitmap */
    void carregaBMP ();

    /** @brief Imprimi os dados do bitmap de foram organizada */
    void toString ();

    /** @brief Retorna o cabeçalho do arquivo bitmap */
    CabecalhoArq getCa ();

    /** @brief Retorna o cabeçalho da imagem bitmap */
    CabecalhoImg getCi ();

    /** @brief Modifica a mensagem */
    void setMensagem (string msg);

    /** @brief Modifica o mapa de pixels */
    void setMapa_pixels (vector<vector<RGB>> mp);

    /** @brief Grava uma imagem bitmap */
    void gravaBMP (string arquivo_saida);

    /** @brief Esconde a mensagem na imagem, usando a esteganografia */
    void esconderMensagem ();

    /** @brief Zera o bit menos significativo de um valor do rgb */
    void zerarUltimoBit (RGB &rgb, int qnt);

    /** @brief Modifica a linha e a coluna para o local do proximo pixel no mapa de pixels */
    void proxPixel (int &linha, int &coluna);

    /** @brief Lê a mensagem gravada no bitmap */
    void lerMensagem ();

};

#endif

