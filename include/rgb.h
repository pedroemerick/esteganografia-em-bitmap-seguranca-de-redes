/**
* @file   rgb.h
* @brief  Arquivo com os atributos e metodos da classe RGB
* @author Pedro Emerick (p.emerick@live.com)
* @since  16/08/2018
* @date   24/08/2018
*/

#ifndef RGB_H
#define RGB_H

#include <string>
using std::string;

/**
* @class   RGB rgb.h
* @brief   Classe que representa um pixel RGB
* @details Os atributos de um RGB são as cores vermelho, verde e azul
*/ 
class RGB 
{
private:
    unsigned char R;        /**< Valor do vermelho do RGB */
    unsigned char G;        /**< Valor do verde do RGB */
    unsigned char B;        /**< Valor do azul do RGB */

public:
    /** @brief Construtor parametrizado */
    RGB (unsigned char red, unsigned char green, unsigned char blue);

    /** @brief Construtor padrao */
    RGB ();

    /** @brief Modifica o valor de vermelho no RGB */
    void setR (unsigned char red);

    /** @brief Retorna o valor de vermelho no RGB */
    unsigned char getR ();

    /** @brief Modifica o valor de verde no RGB */
    void setG (unsigned char green);

    /** @brief Retorna o valor de verde no RGB */
    unsigned char getG ();

    /** @brief Modifica o valor de azul no RGB */
    void setB (unsigned char blue);

    /** @brief Retorna o valor de zaul no RGB */
    unsigned char getB ();

    /** @brief Imprimi os dados do RGB de foram organizada */
    void toString ();
};

#endif