/**
 * @file	cabecalho_img.cpp
 * @brief	Implementacao dos metodos da classe CabecalhoImg
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since   16/08/2018
 * @date    24/08/2018
 * @sa		cabecalho_img.h
 */

#include "cabecalho_img.h"

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <string>
using std::string;

#include <iostream>
using std::cout;
using std::endl;

/**
* @details Os valores são apenas inicializados quando utiliza-se o metodo carregaCabecalho
*/
CabecalhoImg::CabecalhoImg () {
    
}

/**
* @details  Lê o cabecalho da imagem bitmap
* @param    imagem Arquivo para ser lido o cabeçalho
*/
void CabecalhoImg::carregaCabecalho (ifstream &imagem)
{
    imagem.read (reinterpret_cast<char*>(&biSize), sizeof(biSize));
    imagem.read (reinterpret_cast<char*>(&biWidth), sizeof(biWidth));
    imagem.read (reinterpret_cast<char*>(&biHeight), sizeof(biHeight));
    imagem.read (reinterpret_cast<char*>(&biPlanes), sizeof(biPlanes));
    imagem.read (reinterpret_cast<char*>(&biBitCount), sizeof(biBitCount));
    imagem.read (reinterpret_cast<char*>(&biCompression), sizeof(biCompression));
    imagem.read (reinterpret_cast<char*>(&biSizeImage), sizeof(biSizeImage));
    imagem.read (reinterpret_cast<char*>(&biXPelsPerMeter), sizeof(biXPelsPerMeter));
    imagem.read (reinterpret_cast<char*>(&biYPelsPerMeter), sizeof(biYPelsPerMeter));
    imagem.read (reinterpret_cast<char*>(&biClrUsed), sizeof(biClrUsed));
    imagem.read (reinterpret_cast<char*>(&biClrImportant), sizeof(biClrImportant));
}

/**
* @details  Grava o cabecalho da imagem bitmap
* @param    imagem Arquivo para ser gravado o cabeçalho
*/
void CabecalhoImg::gravaCabecalho (ofstream &imagem)
{
    imagem.write (reinterpret_cast<char*>(&biSize), sizeof(biSize));
    imagem.write (reinterpret_cast<char*>(&biWidth), sizeof(biWidth));
    imagem.write (reinterpret_cast<char*>(&biHeight), sizeof(biHeight));
    imagem.write (reinterpret_cast<char*>(&biPlanes), sizeof(biPlanes));
    imagem.write (reinterpret_cast<char*>(&biBitCount), sizeof(biBitCount));
    imagem.write (reinterpret_cast<char*>(&biCompression), sizeof(biCompression));
    imagem.write (reinterpret_cast<char*>(&biSizeImage), sizeof(biSizeImage));
    imagem.write (reinterpret_cast<char*>(&biXPelsPerMeter), sizeof(biXPelsPerMeter));
    imagem.write (reinterpret_cast<char*>(&biYPelsPerMeter), sizeof(biYPelsPerMeter));
    imagem.write (reinterpret_cast<char*>(&biClrUsed), sizeof(biClrUsed));
    imagem.write (reinterpret_cast<char*>(&biClrImportant), sizeof(biClrImportant));
}

/**
* @details  Imprimi os dados do cabeçalho de uma forma organizada e legível
*/
void CabecalhoImg::toString () 
{
    cout << "***** CABEÇALHO IMAGEM *****" << endl;
    cout << "Tamanho do cabeçalho: " << biSize << endl;
    cout << "Largura da imagem em pixels: " << biWidth << endl;
    cout << "Altura da imagem em pixels: " << biHeight << endl;
    cout << "Número de planos de imagem: " << biPlanes << endl;
    cout << "Número de bits usados para cada pixel: " << biBitCount << endl;
    cout << "Tipo de compressão: " << biCompression << endl;
    cout << "Tamanho dos dados: " << biSizeImage << endl;
    cout << "Resolução preferida em pixels por metro horizontal: " << biXPelsPerMeter << endl;
    cout << "Resolução preferida em pixels por metro vertical: " << biYPelsPerMeter << endl;
    cout << "Mapa de Cores Numéricas que são realmente usadas: " << biClrUsed << endl;
    cout << "Número de cores importantes: " << biClrImportant << endl << endl;
}

/**
* @return Altura da imagem em pixels
*/
int CabecalhoImg::getBiHeight () {
    return biHeight;
}

/**
* @return Largura da imagem em pixels
*/
int CabecalhoImg::getBiWidth () {
    return biWidth;
}