/**
 * @file	bmp.cpp
 * @brief	Implementacao dos metodos da classe BMP
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since   16/08/2018
 * @date    24/08/2018
 * @sa		bmp.h
 */

#include "bmp.h"

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;

#include <fstream>
using std::ifstream;
using std::ofstream;

/**
* @details Não é inicializado nenhum valor, até que seja modificado
*/
BMP::BMP () {

}

/**
* @details  Os valores do BMP são recebidos por parâmetro, 
            mas a mensagem é inicializada como uma string vazia
* @param    entrada Arquivo para leitura
* @param    ta Cabeçalho do arquivo bitmap
* @param    ti Cabeçalho da imagem bitmap
*/
BMP::BMP (string entrada, CabecalhoArq ta, CabecalhoImg ti)
{
    ca = ta;
    ci = ti;
    arquivo_entrada = entrada;
    mensagem = "";
}

/**
* @details  Carrega a imagem BMP, carregando os cabeçalhos e o mapa de pixels
*
*   Está com problema, o cabeçalho arquivo não está lendo direito, somente neste metodo,
*   atrapalhando na leitura, não foi descoberto o motivo
*/
void BMP::carregaBMP () 
{
    ifstream imagem (arquivo_entrada);
    
    if (!imagem)
    {
        cerr << "--> Erro ao ler imagem !" << endl;
        exit (1);
    }

    ca.carregaCabecalho(imagem);
    ci.carregaCabecalho(imagem);

    mapa_pixels = vector<vector<RGB>> (ci.getBiHeight(), vector<RGB> (ci.getBiWidth()));
    for (int ii = 0; ii < ci.getBiHeight(); ii++) {
        for (int jj = 0; jj < ci.getBiWidth(); jj++) {
            imagem.read (reinterpret_cast<char*>(&mapa_pixels[ii][jj]), sizeof(mapa_pixels[ii][jj]));
        }
    }

    imagem.close();
}

/**
* @details  Imprimi os cabeçalhos do bitmap
*/
void BMP::toString ()
{
    ca.toString();
    ci.toString();
}

/**
* @return  Retorna o cabeçalho do arquivo bitmap
*/
CabecalhoArq BMP::getCa ()
{
    return ca;
}

/**
* @return  Retorna o cabeçalho da imagem bitmap
*/
CabecalhoImg BMP::getCi ()
{
    return ci;
}

/**
* @details  Modifica a mensagem
* @param    msg Mensagem para mudança
*/
void BMP::setMensagem (string msg) {
    mensagem = msg;
}

/**
* @details  Modifica o mapa de pixels
* @param    mp Novo mapa de pixels
*/
void BMP::setMapa_pixels (vector<vector<RGB>> mp) {
    mapa_pixels = mp;
} 


/**
* @details  Grava uma imagem bitmap
* @param    arquivo_saida Endereço para gravação da imagem bitmap
*/
void BMP::gravaBMP (string arquivo_saida) {

    ofstream imagem (arquivo_saida);

    if (!imagem)
    {
        cerr << "Erro ao gravar imagem com mensagem !!!" << endl;
        exit (1);
    }

    ca.gravaCabecalho(imagem);
    ci.gravaCabecalho(imagem);

    for (int ii = 0; ii < ci.getBiHeight(); ii++) {
        for (int jj = 0; jj < ci.getBiWidth(); jj++) {
            imagem.write (reinterpret_cast<char*>(&mapa_pixels[ii][jj]), sizeof(mapa_pixels[ii][jj]));
        }
    }
    
    
    imagem.close ();

}

/**
* @details  Esconde uma mensagem na imagem bitmap, utilizando esteganografia,
            escondendo um caracter a cada 3 pixels, ao fim da mensagem é gravado 
            o valor ASCII 3 (END OF TEXT), que é um caracter que representa o fim de um texto, 
            para que a imagem não tenha a sua aparência mudada, é modificado apenas
            o bit menos significativo de cada cor RGB do pixel.
*/
void BMP::esconderMensagem () {

    int num_caracteres = (ci.getBiHeight()*ci.getBiWidth()) / 3;
    num_caracteres -= 3;
    
    // Veririca se tem pixel suficiente para gravar a mensagem
    if ((int) mensagem.size() > num_caracteres) {
        cerr << "--> Mensagem muito grande para ser gravada nesta imagem !" << endl;
        exit(1);
    }

    int linha = 0;
    int coluna = 0;

    for (int ii = 0; ii < (int) mensagem.size(); ii++) 
    {
        zerarUltimoBit(mapa_pixels[linha][coluna], 3);

        // É feito um and com os valores das mensagens de acordo com o bit que eu desejo,
        // para que eu pegue apenas o bit que quero gravar, por exemplo, quero gravar o
        // bit menos significativo do caracter, então faço um and com 1, para que 
        // apenas este bit fique com seu valor original e o restante é zerado

        // Caso o caracter fique com valor diferente de zero, será verdadeiro
        if (mensagem[ii] & 1) {
            // Como o bit menos significativo da cor do RGB já está zerado, então 
            // fazemos um or com 1, para que modifique o valor do bit para 1
            mapa_pixels[linha][coluna].setR(mapa_pixels[linha][coluna].getR() | 1);
        } 

        if (mensagem[ii] & 2) {
            mapa_pixels[linha][coluna].setG(mapa_pixels[linha][coluna].getG() | 1);
        } 

        if (mensagem[ii] & 4) {
            mapa_pixels[linha][coluna].setB(mapa_pixels[linha][coluna].getB() | 1);
        } 

        proxPixel(linha, coluna);

        zerarUltimoBit(mapa_pixels[linha][coluna], 3);

        if (mensagem[ii] & 8) {
            mapa_pixels[linha][coluna].setR(mapa_pixels[linha][coluna].getR() | 1);
        } 

        if (mensagem[ii] & 16) {
            mapa_pixels[linha][coluna].setG(mapa_pixels[linha][coluna].getG() | 1);
        } 

        if (mensagem[ii] & 32) {
            mapa_pixels[linha][coluna].setB(mapa_pixels[linha][coluna].getB() | 1);
        }

        proxPixel(linha, coluna);

        zerarUltimoBit(mapa_pixels[linha][coluna], 2);

        if (mensagem[ii] & 64) {
            mapa_pixels[linha][coluna].setR(mapa_pixels[linha][coluna].getR() | 1);
        } 

        if (mensagem[ii] & 128) {
            mapa_pixels[linha][coluna].setG(mapa_pixels[linha][coluna].getG() | 1);
        }

        proxPixel(linha, coluna);
    } 

    // Grava o caracter que define o fim da mensagem, útil para a leitura desta mensagem,
    // podendo saber quando encerra
    char fim = 3;

    zerarUltimoBit(mapa_pixels[linha][coluna], 3);

    if (fim & 1) {
        mapa_pixels[linha][coluna].setR(mapa_pixels[linha][coluna].getR() | 1);
    } 

    if (fim & 2) {
        mapa_pixels[linha][coluna].setG(mapa_pixels[linha][coluna].getG() | 1);
    } 

    if (fim & 4) {
        mapa_pixels[linha][coluna].setB(mapa_pixels[linha][coluna].getB() | 1);
    } 

    proxPixel(linha, coluna);

    zerarUltimoBit(mapa_pixels[linha][coluna], 3);

    if (fim & 8) {
        mapa_pixels[linha][coluna].setR(mapa_pixels[linha][coluna].getR() | 1);
    } 

    if (fim & 16) {
        mapa_pixels[linha][coluna].setG(mapa_pixels[linha][coluna].getG() | 1);
    } 

    if (fim & 32) {
        mapa_pixels[linha][coluna].setB(mapa_pixels[linha][coluna].getB() | 1);
    }

    proxPixel(linha, coluna);

    zerarUltimoBit(mapa_pixels[linha][coluna], 2);

    if (fim & 64) {
        mapa_pixels[linha][coluna].setR(mapa_pixels[linha][coluna].getR() | 1);
    } 

    if (fim & 128) {
        mapa_pixels[linha][coluna].setG(mapa_pixels[linha][coluna].getG() | 1);
    }

}

/**
* @details  Zera o bit menos significativo do valor das cores RGB
* @param    rgb RGB a ser modificado
* @param    qnt Diz quantas cores terão o valor alterado, sempre começando da cor vermelha,
*           então se for 1, vai mudar apenas o vermelho, se for 2, vai muda o vermelho e o verde,
*           se for 3 muda todos os valores
*/
void BMP::zerarUltimoBit (RGB &rgb, int qnt) {

    switch (qnt) {
        case 1:
            rgb.setR(rgb.getR() & 254);
            break;
        case 2:
            rgb.setR(rgb.getR() & 254);
            rgb.setG(rgb.getG() & 254);
            break;
        case 3:
            rgb.setR(rgb.getR() & 254);
            rgb.setG(rgb.getG() & 254);
            rgb.setB(rgb.getB() & 254);
            break;
    }
}

/**
* @details  Modifica os valores para posição do proximo pixel no mapa de pixel
* @param    linha Linha atual no mapa de pixels
* @param    coluna Coluna atual no mapa de pixels
*/
void BMP::proxPixel (int &linha, int &coluna) {
    if ((coluna + 1) == (int) mapa_pixels[0].size()) {
        linha += 1;
        coluna = 0;
    } else {
        coluna += 1;
    }
}

/**
* @details  Lê a mensagem gravada nos pixels da imagem bitmap, e imprimi a mensagem
*/
void BMP::lerMensagem () {

    unsigned char caracter = 0;
    int linha = 0;
    int coluna = 0;

    // Lê os pixels até encontrar o caracter que define o fim da mensagem,
    // no caso o 3 (END OF TEXT)
    do {
        caracter = (mapa_pixels[linha][coluna].getR() & 1) * 1;
        caracter += (mapa_pixels[linha][coluna].getG() & 1) * 2;
        caracter += (mapa_pixels[linha][coluna].getB() & 1) * 4;

        proxPixel(linha, coluna);

        caracter += (mapa_pixels[linha][coluna].getR() & 1) * 8;
        caracter += (mapa_pixels[linha][coluna].getG() & 1) * 16;
        caracter += (mapa_pixels[linha][coluna].getB() & 1) * 32;

        proxPixel(linha, coluna);

        caracter += (mapa_pixels[linha][coluna].getR() & 1) * 64;
        caracter += (mapa_pixels[linha][coluna].getG() & 1) * 128;

        proxPixel(linha,coluna);

        if (caracter != 3)
            mensagem += caracter;
    } while (caracter != 3);

    cout << "--> Mensagem encontrada:" << endl << endl;
    cout << mensagem << endl;
}


