/**
 * @file	cabecalho_arquivo.cpp
 * @brief	Implementacao dos metodos da classe CabecalhoArq
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since   16/08/2018
 * @date    24/08/2018
 * @sa		cabecalho_arquivo.h
 */

#include "cabecalho_arquivo.h"

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <string>
using std::string;

#include <iostream>
using std::cout;
using std::endl;

/**
* @details Os valores são apenas inicializados quando utiliza-se o metodo carregaCabecalho
*/
CabecalhoArq::CabecalhoArq () 
{

}

/**
* @details  Lê o cabecalho do arquivo bitmap
* @param    imagem Arquivo para ser lido o cabeçalho
*/
void CabecalhoArq::carregaCabecalho (ifstream &imagem) 
{
    imagem.read (bfType, sizeof(bfType));
    imagem.read (reinterpret_cast<char*>(&bfSize), sizeof(bfSize));
    imagem.read (reinterpret_cast<char*>(&bfReserved1), sizeof(bfReserved1));
    imagem.read (reinterpret_cast<char*>(&bfReserved2), sizeof(bfReserved2));
    imagem.read (reinterpret_cast<char*>(&bfOffBits), sizeof(bfOffBits));
}

/**
* @details  Grava o cabecalho do arquivo bitmap
* @param    imagem Arquivo para ser gravado o cabeçalho
*/
void CabecalhoArq::gravaCabecalho (ofstream &imagem) 
{
    imagem.write (bfType, sizeof(bfType));
    imagem.write (reinterpret_cast<char*>(&bfSize), sizeof(bfSize));
    imagem.write (reinterpret_cast<char*>(&bfReserved1), sizeof(bfReserved1));
    imagem.write (reinterpret_cast<char*>(&bfReserved2), sizeof(bfReserved2));
    imagem.write (reinterpret_cast<char*>(&bfOffBits), sizeof(bfOffBits));
}

/**
* @details  Imprimi os dados do cabeçalho de uma forma organizada e legível
*/
void CabecalhoArq::toString ()
{
    cout << "***** CABEÇALHO ARQUIVO *****" << endl;
    cout << "Tipo: " << bfType << endl;
    cout << "Tamanho: " << bfSize << endl;
    cout << "Reservado 1: " << bfReserved1 << endl;
    cout << "Reservado 2: " << bfReserved2 << endl;
    cout << "Deslocamento Pixel Data: " << bfOffBits << endl << endl;
}

