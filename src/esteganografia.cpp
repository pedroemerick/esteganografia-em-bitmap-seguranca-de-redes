/**
* @file	    esteganografia.cpp
* @brief	Arquivo com a função principal do programa.
* @author   Pedro Emerick (p.emerick@live.com)
* @since    16/08/2018
* @date     24/08/2018
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <fstream>
using std::ifstream;

#include <vector>
using std::vector;

#include "cabecalho_arquivo.h"
#include "cabecalho_img.h"
#include "bmp.h"
#include "rgb.h"

/**
* @brief Função que recebe o nome/local do arquivo em que se encontra a mensagem
* @param nome_arq Nome do arquivo a ser lido
* @return Mensagem lida do arquivo
*/
string ler_msg (string nome_arq)
{
    ifstream arquivo (nome_arq);
    if (!arquivo)
    {
        cerr << "Erro ao ler mensagem !!!" << endl;
        exit (1);
    }

    string mensagem;
    while (!arquivo.eof()) //enquanto end of file for false continua
    {
        string str_temp;
        getline (arquivo, str_temp);
        mensagem += str_temp;
        mensagem += '\n';
    }

    arquivo.close();

    return mensagem;
}

/**
* @brief Função principal do programa.
*/
int main (int argc, char *argv[]) 
{
    // Faz a validação dos argumentos passados de acordo com a opção desejada
    if (argc != 5 && argc != 3) {
        cerr << "--> Argumentos invalidos !" << endl;
        cerr << "Se deseja esconder a mensagem use: ./prog esconder imagem_entrada.bmp imagem_saida.bmp mensagem.txt" << endl;
        cerr << "Se deseja ler a mensagem use: ./prog ler imagem.bmp" << endl;
        
        exit(1);
    }

    string opcao = argv[1];

    if (opcao != "esconder" && opcao != "ler") {
        cerr << "--> Opção invalida !" << endl;
        cerr << "Se deseja esconder a mensagem use: ./prog esconder imagem_entrada.bmp imagem_saida.bmp mensagem.txt" << endl;
        cerr << "Se deseja ler a mensagem use: ./prog ler imagem.bmp" << endl;

        exit (1);
    } else if (opcao == "esconder" && argc != 5) {
        cerr << "--> Opção invalida !" << endl;
        cerr << "Se deseja esconder a mensagem use: ./prog esconder imagem_entrada.bmp imagem_saida.bmp mensagem.txt" << endl;
        cerr << "Se deseja ler a mensagem use: ./prog ler imagem.bmp" << endl;

        exit (1);
    } else if (opcao == "ler" && argc != 3) {
        cerr << "--> Opção invalida !" << endl;
        cerr << "Se deseja esconder a mensagem use: ./prog esconder imagem_entrada.bmp imagem_saida.bmp mensagem.txt" << endl;
        cerr << "Se deseja ler a mensagem use: ./prog ler imagem.bmp" << endl;

        exit (1);
    }

    // Leitura da imagem está sendo realizada aqui pelo problema do metodo da classe
    // BMP que não foi solucionado para que funcionasse perfeitamente
    ifstream imagem (argv[2]);
    
    if (!imagem)
    {
        cerr << "--> Erro ao ler imagem !" << endl;
        exit (1);
    }

    CabecalhoArq ta;
    ta.carregaCabecalho(imagem);
    
    CabecalhoImg ti;
    ti.carregaCabecalho(imagem);

    BMP bmp (argv[2], ta, ti);
    // bmp.carregaBMP();

    // bmp.toString();

    vector<vector<RGB>> pixels (bmp.getCi().getBiHeight(), vector<RGB> (bmp.getCi().getBiWidth()));

    for (int ii = 0; ii < bmp.getCi().getBiHeight(); ii++) {
        for (int jj = 0; jj < bmp.getCi().getBiWidth(); jj++) {
            imagem.read (reinterpret_cast<char*>(&pixels[ii][jj]), sizeof(pixels[ii][jj]));
        }
    }

    imagem.close();

    bmp.setMapa_pixels(pixels);

    if (opcao == "esconder") {
        string mensagem = ler_msg(argv[4]);
        bmp.setMensagem(mensagem);
        bmp.esconderMensagem(); 
        bmp.gravaBMP(argv[3]);

        cout << "--> Mensagem gravada com sucesso !" << endl;
    } else if (opcao == "ler") {
        bmp.lerMensagem();
    }

    return 0;
}