/**
 * @file	rgb.cpp
 * @brief	Implementacao dos metodos da classe RGB
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since   16/08/2018
 * @date    24/08/2018
 * @sa		rgb.h
 */

#include "rgb.h"

#include <iostream>
using std::cout;
using std::endl;

/**
* @details Os valores são inicializados com zero
*/
RGB::RGB () {
    R = 0;
    G = 0;
    B = 0;
}

/**
* @details  Os valores do RGB são recebidos por parâmetro
* @param    red Valor do vermelho
* @param    green Valor do verde
* @param    blue Valor do azul
*/
RGB::RGB (unsigned char red, unsigned char green, unsigned char blue) {
    R = red;
    G = green;
    B = blue;
}

/**
* @details O metodo modifica o valor do vermelho no RGB
* @param   red Valor do vermelho para o RGB
*/
void RGB::setR (unsigned char red) {
    R = red;
}

/**
* @return Valor do vermelho no pixel
*/
unsigned char RGB::getR () {
    return R;
}

/**
* @details O metodo modifica o valor do verde no RGB
* @param   green Valor do verde para o RGB
*/
void RGB::setG (unsigned char green) {
    G = green;
}

/**
* @return Valor do verde no pixel
*/
unsigned char RGB::getG () {
    return G;
}

/**
* @details O metodo modifica o valor do azul no RGB
* @param   blue Valor do azul para o RGB
*/
void RGB::setB (unsigned char blue) {
    B = blue;
}

/**
* @return Valor do azul no pixel
*/
unsigned char RGB::getB () {
    return B;
}

/**
* @details  Imprimi os dados do RGB de uma forma organizada
*/
void RGB::toString () {
    cout << "[" << R << ", " << G << ", " << B << "]" << endl;
}