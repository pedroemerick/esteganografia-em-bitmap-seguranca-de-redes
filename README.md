## ESTEGANOGRAFIA EM IMAGEM BITMAP

Este repositório contém um programa um que permite esconder e ler uma mensagem de uma imagem bitmap com [Esteganografia](https://pt.wikipedia.org/wiki/Esteganografia).

---

### Considerações Gerais:

O programa permite esconder e ler mensagens utilizando a tabela ASCII como alfabeto, quaisquer outros caracteres que não sejam deste alfabeto usado, serão ignorados, para definir o fim do texto, utiliza-se o valor 3 da tabelas ASCII que significa ETX - End of Text.

Sobre os comandos que o Makefile permite:

* "make clean" = apaga os arquivos objeto e binário;
* "make" = compila o programa;
* "make doc" = gera a documentação do programa, em que será possivel visualizar acesssando a pasta doc e abrindo o arquivo index.html.

---

### Para esconder:
* Compile o programa, usando o comando "make";
* Execute passando os parâmetros corretos:  "./bin/esteganografia esconder imagem.bmp imagem_sec.bmp mensagem.txt".
    * esconder = diz para o programa que você deseja esconder uma mensagem
    * imagem.bmp = arquivo imagem em que deseja esconder a mensagem
    * imagem_sec.bmp = arquivo imagem em que ficará a mensagem escondida
    * mensagem.txt = arquivo que contém a mensagem

---

### Para ler:
* Compile o programa, usando o comando "make";
* Execute passando os parâmetros corretos:  "./bin/esteganografia ler imagem.bmp";
    * ler = diz para o programa que você deseja ler uma mensagem
    * imagem.bmp = arquivo imagem em que contém uma mensagem escondida
* A mensagem encontrada será impressa na saída padrão.



